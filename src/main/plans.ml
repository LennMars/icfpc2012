open Types
open Plan__cond
open Plan__util
open Plan__eval
open Plan__direct
open Plan__move

let plans : plan array =
  [| Plan_0.plan;
     Plan_1.plan;
     Plan_2.plan;
     Plan_SHM.plan_SHM;
     Plan_3.plan;
     Plan_Nearest.plan;
     Plan_4.plan;
     Plan_fine_safety.plan;
     Plan_coarse_safety.plan;
     Plan_compare.plan;
     Plan_fine_safety2.plan;
     Plan_coarse_safety2.plan;
     Plan_compare2.plan;
     Plan_segfault_debug.plan;
     Plan__util.suicide_ideation;
  |]

let conds : cond array =
  [| cond_L; cond_R; cond_U; cond_D; cond_W; cond_A;
     is_lift_reachable;
     is_there_visible_lambda;
     has_razor;
     is_3_beards;
     is_stuck;
  |]

let evals : eval array =
  [|
    Plan_1.eval_naive;
    get_step;
    dist_to_lift;
    get_num_lambda;
    C_funs.eval_mokeo1;
    C_funs.eval_dead;
    C_funs.eval_step;
    C_funs.eval_lambda;
    C_funs.eval_distance_to_goal;
    C_funs.eval_not_get_all_lambda;
    C_funs.eval_get_all_lambda;
    Eval_naive.eval_naive;
    Eval_safety.eval_safety;
    Eval_safety2.eval_safety2;
  |]

let plan_directs : direct array =
  [|
    nearest_lambda;
    nearest_trampoline;
    nearest_lift;
    center;
  |]

let plan_moves : move array =
  [|
    naive_move;
    dijkstra_subj;
  |]

let searches : search array =
  [|
    Plan_1.astar'
  |]

let transmutes : transmute array =
  [|
    Plan__transmute.fall_full;
  |]
