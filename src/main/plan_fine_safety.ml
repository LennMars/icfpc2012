open Types
open Plan__util
open Plan__move

let moves : (plan * cond) array = [|
  plan_L, cond_L;
  plan_R, cond_R;
  plan_U, cond_U;
  plan_D, cond_D;
  plan_W, cond_W; |]

let eval = Eval_safety.eval_safety

let astar' = astar 7 (fun s -> eval s - 1000)

(* collect lambda naively *)
(* then go to goal by dijkstra *)
let plan state =
  if (state.num_lambda > 0) then astar' moves eval state
  else dijkstra_subj (List.hd state.lift_locs) state
