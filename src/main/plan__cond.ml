open Util
open Types
open Plan__util

let is_loc loc state = loc = state.robot_loc

let is_reachable loc state =
  C_funs.dijkstra state.robot_loc loc state
  |> List.is_empty
  |> not

let is_lift_reachable state =
  match Plan__direct.nearest_lift state with
    | None -> false
    | Some lift -> is_reachable lift state

let is_there_visible_lambda state = state.num_visible_lambdas > 0

let has_razor state = state.razors > 0

let is_3_beards state =
  let count = ref 0 in
  let rx, ry = state.robot_loc in
  for x = rx - 1 to rx + 1 do
    for y = ry - 1 to ry + 1 do
      if get state (x, y) = Beard then incr count
    done
  done;
  !count >= 3

let is_stuck state =
  let p c = not (Mine.is_move_valid state c) in
  List.for_all p [L; R; U; D; W]


