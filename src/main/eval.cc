#include "ffi.h"
#include "eval.h"


//死亡時の評価
int eval_dead(state &s){
  if (s.is_dead){
    return deadPoint;
  }
  return 0;
}

//sの時点でのステップ数の評価
int eval_step(state &s){
  return s.step*stepPoint;
}

//sの時点でのlambdaの数の評価
int eval_lambda(state &s){

  return s.num_got_lambdas*lambdaPoint;
}

//ゴールに近づいているか評価
//一番近いLとの距離*distancePointを返す
int eval_distance_to_goal(state &s){

  //初期値はとびきりでかい値

  int  dist =100000000 ;

  for(int i=0;i<s.lift_locs.size();i++){
    int x_dist = s.lift_locs[i].first-s.robot_loc.first;
    if(x_dist<0){
      x_dist*=-1;
    }

    int y_dist = s.lift_locs[i].second-s.robot_loc.second;
    if(y_dist<0){
      y_dist*=-1;
    }

    if(dist>x_dist+y_dist){
      dist=x_dist+y_dist;
    }
  }

  //eval_distance_to_goalを負値にするためにwidthとheight引いてる
  return (dist-s.width-s.height)*2;
}


//lambda残ってるとき
int eval_not_get_all_lambda(state &s){

  return eval_dead(s)+eval_step(s)+eval_lambda(s);
}

//lambda残ってない時
int eval_get_all_lambda(state &s){

  //eval_not_get_all_lambdaと連続性もたせた
  return eval_dead(s)+eval_lambda(s)+eval_step(s)+eval_distance_to_goal(s);
}



int eval(state& s) {
  if(s.num_lambda>0){
    return eval_not_get_all_lambda(s);
  }
  else{
    return eval_get_all_lambda(s);

  }
  return 0;
}

#ifdef __cplusplus
extern "C" {
#endif

  CAMLprim value eval_dead_wrap(value s) {
    CAMLparam1(s);
    state s_;
    get_state(s, s_);
    int x = eval_dead(s_);
    return (Val_int(x));
  }

  CAMLprim value eval_step_wrap(value s) {
    CAMLparam1(s);
    state s_;
    get_state(s, s_);
    int x = eval_step(s_);
    return (Val_int(x));
  }

  CAMLprim value eval_lambda_wrap(value s) {
    CAMLparam1(s);
    state s_;
    get_state(s, s_);
    int x = eval(s_);
    return (Val_int(x));
  }

  CAMLprim value eval_distance_to_goal_wrap(value s) {
    CAMLparam1(s);
    state s_;
    get_state(s, s_);
    int x = eval_distance_to_goal(s_);
    return (Val_int(x));
  }

  CAMLprim value eval_not_get_all_lambda_wrap(value s) {
    CAMLparam1(s);
    state s_;
    get_state(s, s_);
    int x = eval_not_get_all_lambda(s_);
    return (Val_int(x));
  }

  CAMLprim value eval_get_all_lambda_wrap(value s) {
    CAMLparam1(s);
    state s_;
    get_state(s, s_);
    int x = eval_get_all_lambda(s_);
    return (Val_int(x));
  }

  CAMLprim value eval_wrap(value s) {
    CAMLparam1(s);
    state s_;
    get_state(s, s_);
    int x = eval(s_);
    return (Val_int(x));
  }


#ifdef __cplusplus
}
#endif
