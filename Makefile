MAINDIR=src/main
TEAMNUM=96604822

all:
	cd $(MAINDIR); make; cp ./lifter ../../; make clean
	tar -zcvf icfp-$(TEAMNUM).tgz $(MAINDIR) ./lifter README install

clean:
	rm -f icfp-$(TEAMNUM).tgz