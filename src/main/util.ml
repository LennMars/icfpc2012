include UtilPervasives

module List = struct
  include List
  include UtilList
end

module Array = struct
  include Array
  include UtilArray
end

let input ch =
  let rec aux ss =
    try
      let s = input_line ch in aux (s :: ss)
    with
      | End_of_file ->
        if (ch != stdin) then close_in ch;
        List.rev ss in
  aux []

let parse_interactive_input s =
  let open Str in
  let match_digit c = 48 <= int_of_char c && int_of_char c <= 57 in
  let match_invalid_command s = string_match (regexp ".*[^lLrRuUdDwWaAbSsBqQ0-9].*") s 0 in
  if match_invalid_command s then
    raise (Invalid_argument "parse_interactive_input")
  else
    let len = String.length s in
    let p i = i = len - 1 || not (match_digit s.[i + 1]) in
    let rec f i = if p i then i + 1 else f (i + 1) in
    let rec input_aux i cs =
      if i >= len then List.rev cs
      else
        let rep, le =
          if p i then 1, 1
          else String.sub s (i + 1) (f i - i - 1) |> int_of_string, f i - i in
        input_aux (i + le) (List.make rep (Char.uppercase s.[i]) @ cs) in
    input_aux 0 []
