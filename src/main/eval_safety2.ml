open Types
open Plan__util


let eval_dead s = 
    if s.is_dead then (s.num_lambda)*75 + (s.num_got_lambdas)*50 
    else 0

let eval_lift_reachable s = 
    if Plan__cond.is_lift_reachable (Plan__transmute.fall_full s) then 0 
    else (s.num_lambda)*50 + (s.num_got_lambdas)*50

let eval_safety2 s =  
    - s.num_got_lambdas * 25 + s.step + (eval_dead s) + (eval_lift_reachable s)-(C_funs.num_reachable_cells s.robot_loc s)

