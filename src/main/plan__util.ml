open Util
open Types

type loc = int * int
type plan = state -> command list
type cond = state -> bool
type eval = state -> int
type direct = state -> loc option (* define where to go *)
type move = int * int -> plan (* define how to go. [] is unreacheable *)
type search = (plan * cond) array -> eval -> plan (* select preferable plan. Plans may be synthesized in search a*)
type transmute = state -> state

let plan_L _ = [L]
let cond_L s = Mine.is_move_valid s L
let plan_R _ = [R]
let cond_R s = Mine.is_move_valid s R
let plan_U _ = [U]
let cond_U s = Mine.is_move_valid s U
let plan_D _ = [D]
let cond_D s = Mine.is_move_valid s D
let plan_W _ = [W]
let cond_W s = true
let plan_A _ = [A]
let cond_A _ = true

type trace = state * int list

module StateOrd : Set.OrderedType with type t = trace = struct
  type t = trace
  let compare (s1, _) (s2, _) = Types.compare s1 s2
end

module StateSet = Set.Make(StateOrd)

module StateVal : ValuedType with type t = trace = struct
  type t = trace
end

module StateQueue = MakePrioQueue(StateVal)

let dist (x, y) (x1, y1) =
  abs (x - x1) + abs (y - y1)

let astar max_depth aim_val_fun plans_conds eval state =
  let numplan = Array.length plans_conds in
  let plans, conds = Array.separate plans_conds in
  let aim_val = aim_val_fun state in
  let backtrace hist =
    let rec back_aux state acc = function
      | [] -> List.map List.rev acc |> List.rev_flatten
      | i :: is ->
        let cs = plans.(i) state in
        let state = Mine.do_command_all_discard_res state cs in
        back_aux state (cs :: acc) is in
    back_aux state [] hist in
  let rec aux opened closed =
    match StateQueue.top opened with
      | None -> [] (* not found. do nothing *)
      | Some (prio, (s, hist)) ->
        let opened = StateQueue.remove_top opened in
        let closed = StateSet.add (s, hist) closed in
        if prio <= aim_val || List.length hist > max_depth then List.rev hist |> backtrace
        else
          let valid_plans = List.filter (fun n -> conds.(n) s) (List.range 0 (numplan - 1) 1) in
          let commands_for_plans = List.map (fun n -> plans.(n) s) valid_plans in
          let states = List.map (fun n -> copy s, n :: hist) valid_plans in
          let states = List.map2
            (fun (s, hist) cs -> Mine.do_command_all_discard_res s cs, hist)
            states commands_for_plans in
          let states = List.filter (fun s -> not (StateSet.mem s closed)) states in
          let opened =
            let add o (s, hist) =
              if StateSet.mem (s, hist) closed then o
              else StateQueue.insert o (eval s) (s, hist) in
            List.fold_left add opened states in
          aux opened closed in
  aux (StateQueue.singleton (eval state) (copy state, [])) (StateSet.empty)

let astar_oblivious max_depth aim_val_fun plans_conds eval state =
  let numplan = Array.length plans_conds in
  let plans, conds = Array.separate plans_conds in
  let aim_val = aim_val_fun state in
  let backtrace hist =
    let rec back_aux state acc = function
      | [] -> List.map List.rev acc |> List.rev_flatten
      | i :: is ->
        let cs = plans.(i) state in
        let state = Mine.do_command_all_discard_res state cs in
        back_aux state (cs :: acc) is in
    back_aux state [] hist in
  let rec aux opened =
    match StateQueue.top opened with
      | None -> [] (* not found. do nothing *)
      | Some (prio, (s, hist)) ->
        let opened = StateQueue.remove_top opened in
        if prio <= aim_val || List.length hist > max_depth then List.rev hist |> backtrace
        else
          let valid_plans = List.filter (fun n -> conds.(n) s) (List.range 0 (numplan - 1) 1) in
          let commands_for_plans = List.map (fun n -> plans.(n) s) valid_plans in
          let states = List.map (fun n -> copy s, n :: hist) valid_plans in
          let states = List.map2
            (fun (s, hist) cs -> Mine.do_command_all_discard_res s cs, hist)
            states commands_for_plans in
          let opened =
            let add o (s, hist) = StateQueue.insert o (eval s) (s, hist) in
            List.fold_left add opened states in
          aux opened in
  aux (StateQueue.singleton (eval state) (copy state, []))

exception Found_loc of loc

let guruguru_find max_level p state =
  let max_level = min max_level (max state.height state.width) in
  let rx, ry = state.robot_loc in
  let test loc = if p (get state loc) then raise (Found_loc loc) in
  let rec aux level =
    if level > max_level then None else
      try
        for up_r = 0 to level do
          test (rx + up_r, ry + level - up_r)
        done;
        for low_r = 1 to level do
          test (rx + level - low_r, ry - low_r)
        done;
        for low_l = 1 to level do
          test (rx - low_l, ry - level + low_l)
        done;
        for up_l = 1 to level - 1 do
          test (rx - level + up_l, ry + up_l)
        done;
        aux (level + 1)
      with Found_loc loc -> Some loc in
  aux 0

let suicide_ideation state =
  let is_death command =
    let (_, res), _ = Mine.do_command state command in
    match res with
      | Continue | Won -> false
      | _ -> true in
  if List.for_all is_death [L; R; U; D; W; S] then [A]
  else []

let better_plan plan1 plan2 eval state =
  let cs1 = plan1 state and cs2 = plan2 state in
  let s1 = Mine.do_command_all_discard_res state cs1
  and s2 = Mine.do_command_all_discard_res state cs2 in
  if eval s1 < eval s2 then cs1 else cs2
