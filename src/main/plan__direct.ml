open Util
open Types
open Plan__util

let nearest_lambda state =
  if state.num_visible_lambdas <= 0 then None
  else guruguru_find (max state.height state.width) (fun c -> c = Lambda) state

let nearest_trampoline state =
  let me = state.robot_loc in
  match state.trampolines with
    | [] -> None
    | ts -> Some (List.find_min (fun (c, xy) -> dist me xy) ts |> snd)

let nearest_lift state =
  let me = state.robot_loc in
  match state.lift_locs with
    | [] -> None
    | ls -> Some (List.find_min (dist me) state.lift_locs)

let center state =
  Some (state.width / 2, state.height / 2)

