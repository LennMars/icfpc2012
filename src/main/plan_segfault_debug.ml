open Types
open Plan__util
open Plan__move

let moves : (plan * cond) array = [|
  plan_L, cond_L;
  plan_R, cond_R;
  plan_U, cond_U;
  plan_D, cond_D;
  plan_W, cond_W; |]

let eval s = 0
(*  C_funs.eval_mokeo1 s *)

let astar' = astar 10 (fun s -> eval s - 1000)

(* collect lambda naively *)
(* then go to goal by dijkstra *)
let plan state =
  if (state.num_lambda > 0) then (
    print_endline "astar phase start";
    astar' moves eval state)
  else (
    print_endline "dijkstra phase start";
    dijkstra_subj (List.hd state.lift_locs) state
  )
