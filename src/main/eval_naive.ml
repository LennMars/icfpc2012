open Types
open Plan__util

let eval_naive s = 
	if s.is_dead then - (s.num_got_lambdas * 25 - s.step-100)
       else  - (s.num_got_lambdas * 25 - s.step)
