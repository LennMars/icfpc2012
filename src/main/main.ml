open Util
open Types
open C_funs

type mode = Auto of int | Interactive
let in_channel = ref stdin
let debug_channel = ref (open_out "/dev/null")
let mode = ref (Auto 8)
let eval_to_print_num = ref 0

let parse () =
  let open Arg in
  parse [
    "-f", String (fun filename -> in_channel := open_in filename), "";
    "-i", Unit (fun () -> mode := Interactive), "";
    "-o", String (function
      | "stdout" -> debug_channel := stdout
      | "stderr" -> debug_channel := stderr
      | s -> debug_channel := open_out s
    ), "";
    "-m", Int (fun m -> mode := Auto m), "";
    "-e", Int (fun e -> eval_to_print_num := e), "";
  ] (fun s -> ()) ""

let exec () =
  let _ = parse () in
  match !mode with
    | Auto x ->
      let _ = Sys.catch_break true in
      let state = Mine.read (input !in_channel) in
      let plan = Plans.plans.(x) in
      let eval_to_print = Plans.evals.(!eval_to_print_num) in
      let rec aux state commands =
        try
          match commands with
            | [] ->
              aux state (match plan (copy state) with
                | [] -> [W]
                | commands -> commands)
            | l :: r ->
              Mine.print_map !debug_channel state;
              Printf.fprintf !debug_channel "eval: %d\n" (eval_to_print state);
              Printf.printf "%c\n%!" (command_to_char l);
              let (move_res, refresh_res), state = Mine.do_command state l in
              begin match refresh_res with
                | Continue -> aux state r
                | _ -> Mine.print_result !debug_channel state refresh_res end
        with Sys.Break -> aux state commands
      in
      aux state []
    | Interactive ->
      let state = Mine.read (input !in_channel) in
      let eval_to_print = Plans.evals.(!eval_to_print_num) in
      let _ = Mine.print_meta_fixed stdout state in
      let rec aux state history cs =
        Mine.print_meta stdout state;
        Mine.print_map stdout state;
        Printf.fprintf stdout "eval: %d\n" (eval_to_print state);
        print_newline ();
        match cs with
          | [] ->
            aux state history (try parse_interactive_input (read_line ()) with _ -> [])
          | c :: cs ->
            begin match c with
              | 'L' | 'R' | 'U' | 'D' | 'W' | 'A' | 'S' ->
                let snap = copy state in
                let (move_res, refresh_res), state = Mine.do_command state (char_to_command c) in
                begin match refresh_res with
                  | Continue ->
                    aux state (snap :: history) cs
                  | _ ->
                    Mine.print_result stdout state refresh_res;
                    print_newline ();
                    aux state history cs end
              | 'B' -> begin match history with
                  | [] -> raise (Invalid_argument "history")
                  | state_back :: r -> aux state_back r cs end
              | 'Q' -> () (* exit *)
              | _ ->
                prerr_endline "invalid action";
                aux state history cs end
      in
      aux state [] []

let _ = exec ()
