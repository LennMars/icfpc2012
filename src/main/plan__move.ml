open Util
open Types
open Plan__util

let naive_move (x, y) state =
  let (rx, ry) = state.robot_loc in
  let valid_moves = List.filter (Mine.is_move_valid state) move_command_list in
  let rec f = function
    | L :: ms -> if x < rx && (rx - x) >= abs (ry - y) then [L] else f ms
    | R :: ms -> if x > rx && (x - rx) >= abs (ry - y) then [R] else f ms
    | U :: ms -> if y > ry && (y - ry) >= abs (rx - x) then [U] else f ms
    | D :: ms -> if y < ry && (ry - y) >= abs (rx - x) then [D] else f ms
    | _ -> [] (* stuck *) in
  f valid_moves

let dijkstra_subj loc state = C_funs.dijkstra state.robot_loc loc state

let move_conservative move loc state =
  let commands = move loc state in
  let rec aux state acc = function
    | [] -> List.rev acc
    | c :: cs ->
      let (move_res, refresh_res), state = Mine.do_command state c in
      match move_res with
        | Waited -> List.rev acc
        | Moved -> aux state (c :: acc) cs in
  aux state [] commands
