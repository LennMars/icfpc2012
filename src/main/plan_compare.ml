open Types
open Plan__util

let plan = Plan__util.better_plan (Plan_fine_safety.plan) (Plan_coarse_safety.plan) (Eval_safety.eval_safety)
