open Util
open Types
open Plan__util

let plan state =
  if Plan__cond.is_there_visible_lambda state then
    match Plan__direct.nearest_lambda state with
      | None -> []
      | Some loc -> Plan__move.dijkstra_subj loc state
  else []
