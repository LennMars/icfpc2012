#ifndef _EVAL_H_
#define _EVAL_H_

//評価値の定数

//死亡状態の失点
const int deadPoint = 100;

//1ステップの失点
const int stepPoint = 1;

//lambda一個の得点
const int lambdaPoint =-25;

//robotとgoalの直線距離にかける係数
//現在のところ距離が近いほど点が0にちかづく
const int distancePoint = 2;


#endif
