#include "ffi.h"

#ifdef __cplusplus
extern "C" {
#endif

  char get(state& s, int x, int y) {
    x -= s.view.first;
    y -= s.view.second;
    if (0 <= x && x < s.dimx && 0 <= y && y < s.dimy) {
      char c = s.cells[s.dimy * x + y];
      return c;
    } else
      return 'x'; // out_of_map
  }

  char get_unsafe(state& s, int x, int y) {
    return s.cells[s.dimy * (x - s.view.first) + y - s.view.second];
  }

  int cell_num(state &s, int x, int y) {
    return s.dimy * (x - s.view.first) + y - s.view.second;
  }

  PInt loc_of_cell_num(state &s, int n) {
    int x = n / s.dimy;
    int y = n - x * s.dimy;
    return PInt(x, y);
  }

  bool is_transitable(state &s, int x, int y) {
    char c = get(s, x, y);
    return (c == '\\' || c == 'O' || c == 'L' || c == '.' || c == ' ' || (65 <= c && c <= 73) || c == '!');
  }

  bool is_empty(state& s, int x, int y) {
    char c = get(s, x, y);
    return (c == ' ');
  }

  bool is_warp(state& s, int x, int y) {
    char c = get(s, x, y);
    return (65 <= c && c <= 73);
  }

  bool is_target(state& s, int x, int y) {
    char c = get(s, x, y);
    return (49 <= c && c <= 57);
  }

  bool is_rock(state& s, int x, int y) {
    char c = get(s, x, y);
    return (c == '*');
  }

  bool is_pushable_rock(state& s, int x, int y, int dx) {
    char c = get(s, x, y);
    if (c != '*' || (dx != 1 && dx != -1)) return false;
    return (get(s, x + dx, y) == ' ');
  }

  int list_length(value list) {
    CAMLlocal2(l, head);
    l = list;
    int x = 0;
    while (l != Val_emptylist) {
      x += 1;
      l = Field(l, 1);
    }
    return x;
  }

  void get_PInt(value ts, vector<PInt>& v) {
    CAMLlocal1(head);
    while (ts != Val_emptylist) {
      head = Field(ts, 0);
      PInt x = PInt(Get_int(head, 0), Get_int(head, 1));
      dbg("x : %d, %d\n", x.first, x.second);
      v.push_back(x);
      ts = Field(ts, 1);
    }
    dbg("len : %d\n", v.size());
  }

  void get_PcPI(value ts, vector<PcPI>& v) {
    CAMLlocal2(head, snd);
    while (ts != Val_emptylist) {
      head = Field(ts, 0);
      snd = Field(head, 1);
      PcPI x = PcPI(Int_val(Field(head, 0)), PInt(Get_int(snd, 0), Get_int(snd, 1)));
      v.push_back(x);
      ts = Field(ts, 1);
    }
  }

  void get_PPIPI(value ts, vector<PPIPI>& v) {
    CAMLlocal3(head, fst, snd);
    while (ts != Val_emptylist) {
      head = Field(ts, 0);
      fst = Field(head, 0);
      snd = Field(head, 1);
      PPIPI x = PPIPI(PInt(Get_int(fst, 0), Get_int(fst, 1)), PInt(Get_int(snd, 0), Get_int(snd, 1)));
      v.push_back(x);
      ts = Field(ts, 1);
    }
  }

  void get_PPII(value ts, vector<PPII>& v) {
    CAMLlocal2(head, fst);
    while (ts != Val_emptylist) {
      head = Field(ts, 0);
      fst = Field(head, 0);
      PPII x = PPII(PInt(Get_int(fst, 0), Get_int(fst, 1)), Get_int(head, 1));
      v.push_back(x);
      ts = Field(ts, 1);
    }
  }

  int get_state(value s, state& s_) {
    CAMLparam1(s);
    s_.cells = (char*)Data_bigarray_val(Field(s, 0));
    s_.dimx = Bigarray_val(Field(s, 0))->dim[0];
    s_.dimy = Bigarray_val(Field(s, 0))->dim[1];
    s_.width = Get_int(s, 1);
    s_.height = Get_int(s, 2);
    get_PInt(Field(s, 3), s_.lift_locs);
    s_.num_lambda = Get_int(s, 4);
    s_.robot_loc = PInt(Get_int(Field(s, 5), 0), Get_int(Field(s, 5), 1));
    s_.flooding = Get_int(s, 6);
    s_.waterproof = Get_int(s, 7);
    s_.water = Get_int(s, 8);
    s_.num_got_lambdas = Get_int(s, 9);
    s_.step = Get_int(s, 10);
    s_.in_water = Get_int(s, 11);
    s_.is_above_rock = Bool_val(Field(s, 12));
    s_.is_dead = Bool_val(Field(s, 13));
    get_PcPI(Field(s, 14), s_.trampolines);
    get_PcPI(Field(s, 15), s_.targets);
    get_PInt(Field(s, 16), s_.warps);
    s_.is_view = Bool_val(Field(s, 17));
    s_.view = PInt(Get_int(Field(s, 18), 0), Get_int(Field(s, 18), 1));
    s_.view_topright = PInt(Get_int(Field(s, 19), 0), Get_int(Field(s, 19), 1));
    s_.growth = Get_int(s, 20);
    s_.razors = Get_int(s, 21);
    get_PPII(Field(s, 22), s_.growth_count);
    s_.num_visible_lambdas = Get_int(s, 23);
  }

  CAMLprim value print_state(value s) {
    CAMLparam1(s);
    state s_;
    get_state(s, s_);
    printf("%d, %d in C\n", s_.width, s_.height);
    for (int y=s_.height - 1; y >= 0; --y) {
      for (int x = 0; x < s_.width; ++x) {
        printf("%c", get(s_, x, y));
      }
      printf("\n");
    }
    return Val_unit;
  }

#ifdef __cplusplus
}
#endif
