open Util
open Types
open Plan__util

let get_step state = state.step

let dist_to_lift state =
  let (rx, ry) = state.robot_loc in
  List.find_min_val (dist (rx, ry)) state.lift_locs

let get_num_lambda state = state.num_lambda
