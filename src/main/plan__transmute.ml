open Types

let fall_full s =
  let s = copy s in
  s.growth_count <- [];
  let rec aux s =
    let new_s = Mine.do_command_discard_res s W in
    if equals s new_s then s
    else aux new_s in
  aux s
