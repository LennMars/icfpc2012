#ifndef _FFI_H_
#define _FFI_H_

#include <iostream>
#include <string>
#include <cstdio>
#include <utility>
#include <vector>
#include <climits>
#include <queue>
#include <algorithm>

using std::vector;
using std::pair;
using std::priority_queue;

#ifdef DEBUG
#define dbg(...)                                \
  printf(__VA_ARGS__);                          \
  fflush(stdout);
#else
#define dbg(...)
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include <caml/mlvalues.h> // for value conversion macros
#include <caml/memory.h>
#include <caml/alloc.h> // for caml_copy_string eg
#include <caml/bigarray.h>

#define Get_int(v, i) Int_val(Field((v), (i)))

  typedef pair<int, int> PInt;
  typedef pair<char, PInt> PcPI;
  typedef pair<PInt, PInt> PPIPI;
  typedef pair<PInt, int> PPII;

  enum command {L = 0, R = 1, U = 2, D = 3, W = 4, A = 5, S = 6, X = 7};

  struct state {
    char* cells;
    int dimx;
    int dimy;
    int width;
    int height;
    vector<PInt> lift_locs;
    int num_lambda;
    PInt robot_loc;
    int flooding;
    int waterproof;
    int water;
    int num_got_lambdas;
    int step;
    int in_water;
    int is_above_rock;
    int is_dead;
    vector<PcPI> trampolines;
    vector<PcPI> targets;
    vector<PInt> warps;
    int is_view;
    PInt view;
    PInt view_topright;
    int growth;
    int razors;
    vector<PPII> growth_count;
    int num_visible_lambdas;
  };

  int get_state(value s, state& s_);
  char get(state& s, int x, int y);
  char get_unsafe(state& s, int x, int y);
  int cell_num(state &s, int x, int y);
  PInt loc_of_cell_num(state &s, int n);
  bool is_transitable(state &s, int x, int y);
  bool is_empty(state& s, int x, int y);
  bool is_warp(state& s, int x, int y);
  bool is_target(state& s, int x, int y);
  bool is_rock(state& s, int x, int y);
  bool is_pushable_rock(state& s, int x, int y, int dx);


#ifdef __cplusplus
}
#endif

#endif /* _FFI_H_ */

