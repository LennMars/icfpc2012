let (|>) x f = f x

let split_delim s =
  Str.split (Str.regexp ",") s |> List.map int_of_string

let input file =
  let ch = open_in file in
  let rec aux ss =
    try
      let s = input_line ch in aux (s :: ss)
    with
      | End_of_file ->
        close_in ch;
        List.rev ss in
  aux []

let mapname s =
  Str.split (Str.regexp "\\.") s |> List.hd

let plans = ref [1]
let inputs = ref (input "../../inputs/inputs.txt")

let _ =
  Arg.parse [
    "-p", Arg.String (fun s -> plans := split_delim s), "";
    "-i", Arg.String (fun s -> inputs := [s]), "";
  ] (fun s -> ()) "";
  List.iter (fun plan ->
    List.iter (fun input ->
      Unix.system (Printf.sprintf "../main/lifter -f ../../inputs/%s -m %d -o stdout >%s_%d.log"
                     input plan (mapname input) plan) |> ignore
    ) !inputs
  ) !plans

