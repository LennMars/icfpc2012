open Util
open Types

let print_map ch state =
  for i = state.height - 1 downto 0 do
    for j = 0 to state.width - 1 do
      Printf.fprintf ch "%c" (cell_to_char (get state (j, i)));
    done;
    Printf.fprintf ch "\n";
    flush ch
  done

let to_string f xs =
  List.map f xs |> String.concat ", "

let print_meta_fixed ch state =
  Printf.fprintf ch "flooding: %d\n%!" state.flooding;
  Printf.fprintf ch "waterproof: %d\n%!" state.waterproof;
  Printf.fprintf ch "growth: %d\n%!" state.growth;
  let ts_to_string = to_string (fun (c, (x, y)) -> Printf.sprintf "%c, (%d, %d)" c x y) in
  Printf.fprintf ch "trampolines: %s\n%!" (ts_to_string state.trampolines);
  Printf.fprintf ch "targets: %s\n%!" (ts_to_string state.targets);
  let ws_to_string = to_string (fun (c1, c2) -> Printf.sprintf "%c, %c" c1 c2) in
  Printf.fprintf ch "warps: %s\n%!" (ws_to_string state.warps)

let print_meta ch state =
  Printf.fprintf ch "step: %d\n%!" state.step;
  Printf.fprintf ch "water: %d\n%!" state.water;
  Printf.fprintf ch "num_got_lambdas: %d\n%!" state.num_got_lambdas;
  Printf.fprintf ch "num_lambda: %d\n%!" state.num_lambda;
  Printf.fprintf ch "num_visible_lambda: %d\n%!" state.num_visible_lambdas;
  Printf.fprintf ch "in_water: %d\n%!" state.in_water;
  Printf.fprintf ch "is_above_rock: %b\n%!" state.is_above_rock;
  Printf.fprintf ch "is_dead: %b\n%!" state.is_dead;
  Printf.fprintf ch "razors: %d\n%!" state.razors;
  let gc_to_string = to_string (fun ((x, y), i) -> Printf.sprintf "(%d, %d), %d" x y i) in
  Printf.fprintf ch "growth_count: %s\n%!" (gc_to_string state.growth_count)

let partition ss =
  let rec remove_head_blanks = function
    | [] -> []
    | "" :: r -> remove_head_blanks r
    | x -> x in
  let rec aux ss map_desc =
    match ss with
      | [] -> List.rev map_desc, []
      | "" :: r -> List.rev map_desc, r
      | l :: r -> aux r (l :: map_desc) in
  aux (remove_head_blanks ss) []

let read ss =
  let map_desc, meta = partition ss in
  let width = List.find_max_val String.length map_desc in
  let height = List.length map_desc in
  let state = create width height in
  let flooding = ref 0 in
  let waterproof = ref 10 in
  let water = ref 0 in
  let warps = ref [] in
  let growth = ref 25 in
  let razors = ref 0 in
  let read_meta meta =
    let check s =
      let sp = Str.split (Str.regexp " ") s in
      match List.length sp with
        | 2 ->
          let value = int_of_string (List.nth sp 1) in
          begin match List.nth sp 0 with
            | "Water" -> water := value
            | "Flooding" -> flooding := value
            | "Waterproof" -> waterproof := value
            | "Growth" -> growth := value
            | "Razors" -> razors := value
            | _ -> () end
        | 4 ->
          begin match sp with
            | ["Trampoline"; tram; "targets"; target] -> warps := (tram.[0], target.[0]) :: !warps
            | _  -> () end
        | _ -> () in
    List.iter check meta in
  let lift_locs = ref [] in
  let num_lambda = ref 0 in
  let robot_loc = ref (0, 0) in
  let trampolines = ref [] in
  let targets = ref [] in
  let growth_count = ref [] in
  let num_visible_lambdas = ref 0 in
  let rec read_mapdesc i = function
    | [] -> ()
    | l :: r ->
      for x = 0 to String.length l - 1 do
        let y = height - i - 1 in
        let cell = char_to_cell l.[x] in
        set state (x, y) cell;
        match cell with
          | Robot -> robot_loc := x, y
          | Lambda ->
            num_lambda := !num_lambda + 1;
            num_visible_lambdas := !num_visible_lambdas + 1
          | Closed -> lift_locs := (x, y) :: !lift_locs
          | Trampoline c -> trampolines := (c, (x, y)) :: !trampolines
          | Target c -> targets := (c, (x, y)) :: !targets
          | Beard -> growth_count := ((x, y), 0) :: !growth_count
          | HOR -> num_lambda := !num_lambda + 1
          | _ -> ()
      done;
      read_mapdesc (i + 1) r in
  read_meta meta;
  read_mapdesc 0 map_desc;
  {
    state with
    lift_locs = !lift_locs;
    num_lambda = !num_lambda;
    robot_loc = !robot_loc;
    flooding = !flooding;
    waterproof = !waterproof;
    water = !water;
    is_above_rock =
      (get state (dest state.robot_loc U) = Rock || get state (dest state.robot_loc U) = HOR);
    trampolines = !trampolines;
    targets = !targets;
    warps = !warps;
    growth = !growth;
    razors = !razors;
    growth_count = !growth_count;
    num_visible_lambdas = !num_visible_lambdas;
  }

let is_move_valid state command =
  let dest_x, dest_y = dest state.robot_loc command in
  let cell = get state (dest_x, dest_y) in
  match cell with
    | Empty | Earth | Robot | Lambda | Open | Trampoline _ | Razor -> true
    | Out_of_map | Wall | Closed | Target _ | Beard -> false
    | Rock | HOR ->
      let far_x, far_y = dest (dest_x, dest_y) command in
      begin match command, get state (far_x, far_y) with
        | L, Empty | R, Empty -> true
        | _ -> false end

let move state = function
  | S ->
    if state.razors > 0 then
      let x, y = state.robot_loc in
      for i = x - 1 to x + 1 do
        for j = y - 1 to y + 1 do
          if get state (i, j) = Beard then (
            set state (i, j) Empty;
            state.growth_count <- List.filter (fun (loc, _) -> loc <> (i, j)) state.growth_count
          )
        done
      done;
      state.razors <- state.razors - 1;
      Waited
    else Waited
  | command ->
    let prev_x, prev_y = state.robot_loc in
    let dest_x, dest_y = dest state.robot_loc command in
    let cell = get state (dest_x, dest_y) in
    let res = match cell with
      | Empty | Earth | Lambda | Open | Razor ->
        set state (prev_x, prev_y) Empty;
        set state (dest_x, dest_y) Robot;
        state.robot_loc <- (dest_x, dest_y);
        begin match cell with
          | Razor ->
            state.razors <- state.razors + 1
          | Lambda ->
            state.num_got_lambdas <- state.num_got_lambdas + 1;
            state.num_lambda <- state.num_lambda - 1;
            state.num_visible_lambdas <- state.num_visible_lambdas - 1
          | _ -> () end;
        Moved
      | Trampoline char -> begin
        try let target_char = List.assoc char state.warps in
            let t_x, t_y = List.assoc target_char state.targets in
            set state (prev_x, prev_y) Empty;
            set state (dest_x, dest_y) Empty;
            set state (t_x, t_y) Robot;
            state.robot_loc <- (t_x, t_y);
            let tram_chars = List.filter (fun (_, c) -> c = target_char) state.warps |> List.map fst in
            let delete tram_char =
              let x, y = List.assoc tram_char state.trampolines in
              set state (x, y) Empty in
            List.iter delete tram_chars;
            state.trampolines <- List.filter (fun (c, _) -> List.exists (( <> ) c) tram_chars)
              state.trampolines;
            state.targets <- List.filter (fun (c, _) -> c <> target_char) state.targets;
            Moved
        with Not_found ->
          prerr_endline "Trampoline Not found";
          Waited end
      | Rock | HOR ->
        let far_x, far_y = dest (dest_x, dest_y) command in
        begin match command, get state (far_x, far_y) with
          | L, Empty | R, Empty ->
            set state (prev_x, prev_y) Empty;
            set state (dest_x, dest_y) Robot;
            set state (far_x, far_y) cell; (* Rock | HOR *)
            state.robot_loc <- (dest_x, dest_y);
            Moved
          | _ ->
            Waited end
      | Out_of_map | Robot | Wall | Closed | Target _ | Beard -> Waited in
    let _ =
      state.is_above_rock <-
        (get state (dest state.robot_loc U) = Rock || get state (dest state.robot_loc U) = HOR);
    in
    res

let rule1 old state (x, y) =
  match get old (x, y), get old (x, y - 1) with
    | Rock, Empty ->
      set state (x, y) Empty;
      set state (x, y - 1) Rock;
      true
    | HOR, Empty ->
      set state (x, y) Empty;
      let below = get old (x, y - 2) in
      if below <> Empty && below <> Robot then (
        set state (x, y - 1) Lambda;
        state.num_visible_lambdas <- state.num_visible_lambdas + 1
      ) else
        set state (x, y - 1) HOR;
      true
    | _ ->
      false

let rule2 old state (x, y) =
  match get old (x, y), get old (x, y - 1), get old (x + 1, y), get old (x + 1, y - 1) with
    | Rock, Rock, Empty, Empty | Rock, HOR, Empty, Empty ->
      set state (x, y) Empty;
      set state (x + 1, y - 1) Rock;
      true
    | HOR, Rock, Empty, Empty | HOR, HOR, Empty, Empty ->
      set state (x, y) Empty;
      let below = get old (x + 1, y - 2) in
      if below <> Empty && below <> Robot then (
        set state (x + 1, y - 1) Lambda;
        state.num_visible_lambdas <- state.num_visible_lambdas + 1
      ) else
        set state (x + 1, y - 1) HOR;
      true
    | _ ->
      false

let rule3 old state (x, y) =
  match get old (x, y), get old (x, y - 1), get old (x - 1, y), get old (x - 1, y - 1) with
    | Rock, Rock, Empty, Empty | Rock, HOR, Empty, Empty ->
      begin match get old (x + 1, y), get old (x + 1, y - 1) with
        | Empty, Empty -> false
        | _ ->
          set state (x, y) Empty;
          set state (x - 1, y - 1) Rock;
          true end
    | HOR, Rock, Empty, Empty | HOR, HOR, Empty, Empty ->
      begin match get old (x + 1, y), get old (x + 1, y - 1) with
        | Empty, Empty -> false
        | _ ->
          set state (x, y) Empty;
          let below = get old (x - 1, y - 2) in
          if below <> Empty && below <> Robot then (
            set state (x - 1, y - 1) Lambda;
            state.num_visible_lambdas <- state.num_visible_lambdas + 1
          ) else
            set state (x - 1, y - 1) HOR;
          true end
    | _ -> false

let rule4 old state (x, y) =
  match get old (x, y), get old (x, y - 1), get old (x + 1, y), get old (x + 1, y - 1) with
    | Rock, Lambda, Empty, Empty ->
      set state (x, y) Empty;
      set state (x + 1, y - 1) Rock;
      true
    | HOR, Lambda, Empty, Empty ->
      set state (x, y) Empty;
      let below = get old (x + 1, y - 2) in
      if below <> Empty && below <> Robot then (
        set state (x + 1, y - 1) Lambda;
        state.num_visible_lambdas <- state.num_visible_lambdas + 1
      ) else
        set state (x + 1, y - 1) HOR;
      true
    | _ ->
      false

let rule5 old state (x, y) =
  if (get old (x, y) = Closed) && (old.num_lambda = 0) then begin
    set state (x, y) Open;
    true end
  else
    false

let rule6 old state (x, y) =
  try
    if get old (x, y) = Beard then
      let gen_step = List.assoc (x, y) old.growth_count in
      if (old.step + 1 - gen_step) mod old.growth = 0 then begin
        for i = x - 1 to x + 1 do
          for j = y - 1 to y + 1 do
            if i = x && j = y then () else (
              if get old (i, j) = Empty then begin
                set state (i, j) Beard;
                state.growth_count <- ((i, j), old.step + 1) :: state.growth_count
              end
            )
          done
        done;
        true end
      else false
    else false
  with Not_found -> false


let refresh old_state state loc =
  if rule1 old_state state loc then ()
  else if rule2 old_state state loc then ()
  else if rule3 old_state state loc then ()
  else if rule4 old_state state loc then ()
  else if rule5 old_state state loc then ()
  else if rule6 old_state state loc then ()
  else ()

let do_command state command =
  if state.is_dead then (Waited, Already_dead), state
  else if state.step > state.width * state.height then (Waited, Aborted), state
  else
    let move_res = move state command in
    let old_state = state in
    let state = copy old_state in
    let _ =
      let x1, y1 = state.view and x2, y2 = state.view_topright in
      for y = y1 to y2 - 1 do
        for x = x1 to x2 - 1do
          refresh old_state state (x, y)
        done
      done;
      state.step <- state.step + 1;
      state.in_water <- if snd state.robot_loc < state.water then state.in_water + 1 else 0;
      if state.flooding != 0 && state.step mod state.flooding = 0 then
        state.water <- state.water + 1 in
    let refresh_res =
      if
        state.num_lambda = 0 &&
        List.exists (fun loc -> state.robot_loc = loc) state.lift_locs
      then Won
      else if
          (not state.is_above_rock &&
             (get state (dest state.robot_loc U) = Rock
             || get state (dest state.robot_loc U) = HOR)
          ) || (state.in_water > state.waterproof)
      then (state.is_dead <- true; Lose)
      else if command = A then Aborted
      else Continue in
    (move_res, refresh_res), state

let do_command_discard_res state command = snd (do_command state command)

let do_command_all state commands =
  let rec aux res state = function
    | [] -> res, state
    | c :: cs ->
      let res, state = do_command state c in
      aux res state cs in
  aux (Waited, Continue) commands

let do_command_all_discard_res state commands =
  List.fold_left (fun s c -> do_command_discard_res s c) state commands

let score state res =
  let raw = state.num_got_lambdas * 25 - state.step * 1 in
  raw + match res with
    | Continue -> 0
    | Won -> state.num_got_lambdas * 50
    | Lose -> 0
    | Aborted -> state.num_got_lambdas * 25
    | Already_dead -> 0

let print_result ch state res =
  Printf.fprintf ch "game over. %s\n" (refresh_result_to_string res);
  print_map ch state;
  let score = score state res in
  Printf.fprintf ch "Score: %d\n" score
