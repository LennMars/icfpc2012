#include <iostream>
#include <string>
#include <cstdio>

#ifdef __cplusplus
extern "C" {
#endif

#include <caml/mlvalues.h> // for value conversion macros
#include <caml/memory.h>
// #include <caml/callback.h> // needed when calling OCaml function from C
#include <caml/alloc.h> // for caml_copy_string eg

// follow templates below, that use CAMLprim, CAMLparamN, CAMLlocalN and CAMLreturn.
// detail : http://caml.inria.fr/pub/docs/manual-ocaml/manual032.html
// or http://www.geocities.jp/harddiskdive/ocaml-wrapping-c/ocaml-wrapping-c.html

// CAMLprim macro ensures following function is exported and accessible from OCaml

CAMLprim value addxxx (value str) { // string -> string
  CAMLparam1(str); // CAMLfoo macros prevent OCaml from GC
  CAMLlocal1(res);

  std::string cstr = String_val(str);
  cstr += "xxx";

  res = caml_copy_string(cstr.c_str());
  CAMLreturn(res);
}

CAMLprim value inc(value i) { // int -> int
    CAMLparam1(i);
    int j = Int_val(i);
    ++j;
    CAMLreturn(Val_int(j));
}

CAMLprim value mult2(value x) { // float -> float
  CAMLparam1(x);
  CAMLlocal1(y);
  y = caml_copy_double(Double_val(x) * 2.0);
  CAMLreturn(y);
}

CAMLprim value sum_int_array(value xs) { // int array -> int
  CAMLparam1(xs);
  CAMLlocal1(x);
  int len = Wosize_val(xs);
  int y = 0;
  for(int i = 0; i < len; ++i) {
    x = Field(xs, i);
    y += Int_val(x);
  }
  CAMLreturn(Val_int(y));
}

CAMLprim value sum_int_tuple(value x) { // int * int -> int
  CAMLparam1(x);
  CAMLreturn(Val_int(Int_val(Field(x, 0)) + Int_val(Field(x, 1))));
}

CAMLprim value sum_float_array(value xs) { // float array -> float
  CAMLparam1(xs);
  CAMLlocal1(res);
  int len = Wosize_val(xs) / Double_wosize; // OCaml float array is unboxed
  double y = 0.0;
  for(int i = 0; i < len; ++i) {
    y += Double_field(xs, i);
  }
  res = caml_copy_double(y);
  CAMLreturn(res);
}

CAMLprim value sum_int_list(value xs) { // int list -> int
  CAMLparam1(xs);
  CAMLlocal1(head);
  int x = 0;
  while (xs != Val_emptylist) {
    head = Field(xs, 0);  /* accessing the head */
    x += Int_val(head);
    xs = Field(xs, 1);  /* point to the tail for next loop */
  }
  CAMLreturn(Val_int(x));
}

CAMLprim value make_array(value x, value len) { // 'a -> int -> 'a array
  CAMLparam2(x, len);
  CAMLlocal1(array);
  int length = Int_val(len);
  array = caml_alloc(length, 0);
  for(int i = 0; i < length; ++i) {
    Store_field(array, i, x);
  }
  CAMLreturn(array);
}

CAMLprim value make_tuple(value x) { // 'a -> 'a * 'a * 'a
  CAMLparam1(x);
  CAMLlocal1(tuple);
  tuple = caml_alloc(3, 0);
  Store_field(tuple, 0, x);
  Store_field(tuple, 1, x);
  Store_field(tuple, 2, x);
  CAMLreturn(tuple);
}

CAMLprim value make_float_array(value x, value len) { // float -> int -> float array
  CAMLparam2(x, len);
  CAMLlocal1(farray);
  double y = Double_val(x);
  int length = Int_val(len);
  farray = caml_alloc(length * Double_wosize, Double_array_tag);
  for (int i = 0; i < length; ++i) {
    Store_double_field(farray, i, y);
  }
  CAMLreturn(farray);
}

CAMLprim value make_list(value x, value len) { // 'a -> int -> 'a list
  CAMLparam2(x, len);
  CAMLlocal2(list, cons);
  int length = Int_val(len);
  list = Val_emptylist;
  for (int i = length; i > 0; --i) {
    cons = caml_alloc(2, 0);
    Store_field(cons, 0, x); // head
    Store_field(cons, 1, list);// tail
    list = cons;
  }
  CAMLreturn(list);
}

CAMLprim value print_variant(value x) { // A | B of int | C of float | D -> unit
  CAMLparam1(x);
  if (Is_long(x)) {
    switch (Int_val(x)) {
    case 0 : printf("A\n"); break;
    case 1 : printf("D\n"); break;
    }
  } else {
    switch (Tag_val(x)) {
    case 0 : printf("B %d\n", Int_val(Field(x, 0))); break;
    case 1 : printf("C %f\n", Double_val(Field(x, 0))); break;
    }
  }
  return Val_unit;
}

void nothing() {
    CAMLparam0();
    CAMLreturn0;
}

#ifdef __cplusplus
}
#endif
