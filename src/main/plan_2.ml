open Types
open Plan__util

let moves : (plan * cond) array = [|
  plan_L, cond_L;
  plan_R, cond_R;
  plan_U, cond_U;
  plan_D, cond_D;
  plan_W, cond_W; |]

let eval = C_funs.eval_mokeo1

let astar' = astar 3 (fun s -> eval s - 10)

let plan state =
  astar' moves eval state
