#include "ffi.h"

#ifdef __cplusplus
extern "C" {
#endif

  struct edge {
    int dest;
    int cost;
  };

  const int empty_cost = 3;
  const int warp_cost = 0;
  const int other_transitable_cost = 10;
  const int pushable_rock_cost = 30;
  const int rock_to_horizontal_cost = 20;
  const int horizontal_move_cost_offset = 1;

  int transitable_cost (state& s, int x, int y) {
    if (is_empty(s, x, y)) return empty_cost;
    else if (is_warp(s, x, y)) return warp_cost;
    else if (is_transitable(s, x, y)) return other_transitable_cost;
    else return INT_MAX;
  }

  void make_graph(state& s, vector<edge>* v) {
    for (int x = s.view.first; x < s.view_topright.first; ++x) {
      for (int y = s.view.second; y < s.view_topright.second; ++y) {
        if (get(s, x, y) == '#' || get(s, x, y) == 'W') continue;
        int origin = cell_num(s, x, y);
        // above, below
        for(int dy = -1; dy <= 1; dy+=2) {
          if (is_transitable(s, x, y + dy)) {
            edge e;
            e.dest = cell_num(s, x, y + dy);
            e.cost = transitable_cost(s, x, y + dy);
            v[origin].push_back(e);
          }
        }
        // yoko
        for(int dx = -1; dx <= 1; dx+=2) {
          if (is_transitable(s, x + dx, y)) {
            edge e;
            e.dest = cell_num(s, x + dx, y);
            if (is_rock(s, x, y)) {
              e.cost = rock_to_horizontal_cost;
            } else {
              e.cost = transitable_cost(s, x + dx, y) + horizontal_move_cost_offset;
            }
            v[origin].push_back(e);
          } else if (is_pushable_rock(s, x + dx, y, dx)) {
            edge e;
            e.dest = cell_num(s, x + dx, y);
            e.cost = pushable_rock_cost;
            v[origin].push_back(e);
          }
        }
        // warp
        if (is_warp(s, x, y)) {
          char tram = get(s, x, y);
          dbg("warp from : %c, %d, %d\n", tram, x, y);
          char targ = 'x';
          for (vector<PInt>::iterator it = s.warps.begin(); it != s.warps.end(); ++it) {
            dbg("%c, %c; ", it->first, it->second);
            if (it->first == tram) targ = it->second;
          }
          if (targ != 'x') { // target char found
            dbg("to target %c\n", targ);
            PInt targ_loc = PInt(-1, -1);
            for (vector<PcPI>::iterator it = s.targets.begin(); it != s.targets.end(); ++it) {
              if (it->first == targ) targ_loc = it->second;
            }
            if (targ_loc != PInt(-1, -1)) { // target loc found
              dbg("targ loc : %d, %d\n", targ_loc.first, targ_loc.second);
              edge e;
              e.dest = cell_num(s, targ_loc.first, targ_loc.second);
              e.cost = warp_cost;
              v[origin].push_back(e);
            }
          }
        }

      } // y loop end
    }
  }

  void print_edges(state& s, vector<edge>* edges) {
    int num_vertex = s.dimx * s.dimy;
    for (int v = 0; v < num_vertex; ++v) {
      PInt loc = loc_of_cell_num(s, v);
      if (!edges[v].size()) continue;
      printf("(%d, %d) : ", loc.first, loc.second);
      for (vector<edge>::iterator e = edges[v].begin(); e != edges[v].end(); ++e) {
        PInt d = loc_of_cell_num(s, e->dest);
        printf("(%d, %d) %d; ", d.first, d.second, e->cost);
      }
      printf("\n");
    }
  }

  enum command move_to_command(state& s, PInt prev, PInt next) {
    int px = prev.first;
    int py = prev.second;
    int nx = next.first;
    int ny = next.second;
    if (ny == py) {
      if (nx - px == 1) return R;
      else if (nx - px == -1) return L;
      else return X;
    } else if (nx == px) {
      if (ny - py == 1) return U;
      else if (ny - py == -1) return D;
      else return X;
    } else if (is_target(s, next.first, next.second)) {
      char targ = get(s, next.first, next.second);
      char tram = 'x';
      for (vector<PInt>::iterator it = s.warps.begin(); it != s.warps.end(); ++it) {
        if (it->second == targ) tram = it->first;
      }
      if (tram != 'x') { // tram char found
        PInt tram_loc = PInt(-1, -1);
        for (vector<PcPI>::iterator it = s.trampolines.begin(); it != s.trampolines.end(); ++it) {
          if (it->first == targ) tram_loc = it->second;
        }
        if (tram_loc != PInt(-1, -1)) { // tram loc found
          command c = move_to_command(s, prev, tram_loc);
          return c;
        }
      }
    } else {
      return X;
    }
  }

  void dijkstra_main(state& s, PInt start, int* d, int* prev) {
    int num_vertex = s.dimx * s.dimy;
    vector<edge>* edges = new vector<edge>[num_vertex];

    make_graph(s, edges);
    #ifdef DEBUG
    print_edges(s, edges);
    #endif
    for (int v = 0; v < num_vertex; ++v) {
      d[v] = INT_MAX;
      prev[v] = -1;
    }

    int startnum = cell_num(s, start.first, start.second);
    priority_queue<PInt, vector<PInt>, std::greater<PInt> > queue;
    d[startnum] = 0;
    queue.push(PInt(0, startnum));

    while (!queue.empty()) {
      PInt p = queue.top();
      queue.pop();
      int v = p.second;
      if (d[v] < p.first) continue;
      for (int i = 0; i < edges[v].size(); ++i) {
        edge e = edges[v][i];
        if (d[e.dest] > d[v] + e.cost) {
          d[e.dest] = d[v] + e.cost;
          prev[e.dest] = v;
          queue.push(PInt(d[e.dest], e.dest));
        }
      }
    }

    #ifdef DEBUG
    for(int i=0; i < num_vertex; ++i) {
      if (prev[i] != -1) {
        PInt p = loc_of_cell_num(s, prev[i]);
        PInt pi = loc_of_cell_num(s, i);
        printf("path : (%d, %d)->(%d, %d)\n", p.first, p.second, pi.first, pi.second);
        fflush(stdout);
      }
    }
    #endif

    delete[] edges;
  }

  int num_reachable_cells(state& s, PInt start) {
    int num_vertex = s.dimx * s.dimy;
    int* d = (int*)malloc(num_vertex * sizeof(int));
    int* prev = (int*)malloc(num_vertex * sizeof(int));

    dijkstra_main(s, start, d, prev);

    int count = 0;
    for (int i = 0; i < num_vertex; ++i) {
      if (d[i] != INT_MAX) ++count;
    }

    return count;
  }

  vector<command> dijkstra(state& s, PInt start, PInt goal) {
    int num_vertex = s.dimx * s.dimy;
    int* d = (int*)malloc(num_vertex * sizeof(int));
    int* prev = (int*)malloc(num_vertex * sizeof(int));

    dijkstra_main(s, start, d, prev);

    int goalnum = cell_num(s, goal.first, goal.second);
    vector<command> cs;
    if (d[goalnum] == INT_MAX) {
      delete d;
      delete prev;
      return cs; // cannot goal
    } else {
      vector<PInt> locs;
      for (int t = goalnum; t != -1; t = prev[t]) locs.push_back(loc_of_cell_num(s, t));
      reverse(locs.begin(), locs.end());
      for (vector<PInt>::iterator it = locs.begin(); it + 1 != locs.end(); ++it) {
        enum command c = move_to_command(s, *it, *(it + 1));
        if (c != X) cs.push_back(c);
        else return cs; // command is unknown
      }
      delete d;
      delete prev;
      return cs;
    }
  }

  CAMLprim value dijkstra_wrap(value start, value goal, value s) {
    CAMLparam3(start, goal, s);
    CAMLlocal2(list, cons);
    state s_;
    get_state(s, s_);
    PInt start_ = PInt(Get_int(start, 0), Get_int(start, 1));
    PInt goal_ = PInt(Get_int(goal, 0), Get_int(goal, 1));
    vector<command> cs = dijkstra(s_, start_, goal_);
    int len = cs.size();
    list = Val_emptylist;
    for (int i = len - 1; i >= 0; --i) {
      cons = caml_alloc(2, 0);
      Store_field(cons, 0, Val_int(cs[i]));
      Store_field(cons, 1, list);
      list = cons;
    }
    CAMLreturn(list);
  }

  CAMLprim value num_reachable_cells_wrap(value start, value s) {
    CAMLparam2(start, s);
    state s_;
    get_state(s, s_);
    PInt start_(Get_int(start, 0), Get_int(start, 1));
    int count = num_reachable_cells(s_, start_);
    CAMLreturn(Val_int(count));
  }

#ifdef __cplusplus
}
#endif
