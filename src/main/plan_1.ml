open Types
open Plan__util

let moves : (plan * cond) array = [|
  plan_L, cond_L;
  plan_R, cond_R;
  plan_U, cond_U;
  plan_D, cond_D;
  plan_W, cond_W; |]

let eval_naive s = - (s.num_got_lambdas * 25 - s.step)

let astar' = astar 10 (fun s -> eval_naive s - 1000)

let plan state =
  astar' moves eval_naive state
