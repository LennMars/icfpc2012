open Types

external print_state_in_c : state -> unit = "print_state"
external dijkstra : int * int -> int * int -> state -> command list = "dijkstra_wrap"
external num_reachable_cells : int * int -> state -> int = "num_reachable_cells_wrap"
external eval_mokeo1 : state -> int = "eval_wrap"
external eval_dead : state -> int = "eval_dead_wrap"
external eval_step : state -> int = "eval_step_wrap"
external eval_lambda : state -> int = "eval_lambda_wrap"
external eval_distance_to_goal : state -> int = "eval_distance_to_goal_wrap"
external eval_not_get_all_lambda : state -> int = "eval_not_get_all_lambda_wrap"
external eval_get_all_lambda : state -> int = "eval_get_all_lambda_wrap"


