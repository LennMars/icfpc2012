open Util
open Bigarray

type cell =
  | Robot | Wall | Rock | Lambda | Closed | Open | Earth | Empty
  | Trampoline of char | Target of char | Beard | Razor | HOR | Out_of_map

exception Invalid_move
exception Unknown_char of string
exception Setting_out_of_map

let char_to_cell = function
  | 'R' -> Robot
  | '#' -> Wall
  | '*' -> Rock
  | '\\' -> Lambda
  | 'L' -> Closed
  | 'O' -> Open
  | '.' -> Earth
  | ' ' -> Empty
  | 'A'..'I' as c -> Trampoline c
  | '1'..'9' as c -> Target c
  | 'W' -> Beard
  | '!' -> Razor
  | '@' -> HOR
  | x -> raise (Unknown_char (Printf.sprintf " %c " x))

let cell_to_char = function
  | Robot -> 'R'
  | Wall -> '#'
  | Rock -> '*'
  | Lambda -> '\\'
  | Closed -> 'L'
  | Open -> 'O'
  | Earth -> '.'
  | Empty -> ' '
  | Trampoline c -> c
  | Target c -> c
  | Beard -> 'W'
  | Razor -> '!'
  | HOR -> '@'
  | Out_of_map -> 'x'

type cells = (char, int8_unsigned_elt, c_layout) Array2.t

type state = {
  cells : cells;
  width : int;
  height : int;
  lift_locs : (int * int) list; (* location of lifts *)
  mutable num_lambda : int; (* the number of remaining lambdas. includes invisible and disappeared ones *)
  mutable robot_loc : int * int; (* location of the robot *)
  flooding : int;
  waterproof : int;
  mutable water : int; (* this and the above two are flooding parameter. see weather.pdf *)
  mutable num_got_lambdas : int; (* the number of lambdas the robot already got *)
  mutable step : int; (* game step *)
  mutable in_water : int; (* consecutive turns staying in water *)
  mutable is_above_rock : bool; (* whether a rock is above the robot. needed for Losing judge *)
  mutable is_dead : bool; (* whether the robot was broken in the past *)
  mutable trampolines : (char * (int * int)) list; (* char and loc of trampolines *)
  mutable targets : (char * (int * int)) list;
  warps : (char * char) list; (* warp char correspondence relation *)
  is_view : bool; (* extracted partial information from full map of width * height *)
  view : int * int; (* location of bottom left of the view in the full map *)
  view_topright : int * int; (* same as above *)
  growth : int;
  mutable razors : int;
  mutable growth_count : ((int * int) * int) list; (* snd is generated turn so that not need decrement every turn *)
  mutable num_visible_lambdas : int;
}

exception Found

let equals s1 s2 =
  s1.width = s2.width && s1.height = s2.height &&
  match s1.is_view, s2.is_view with
    | false, false -> begin
      try
        for x = 0 to s1.width - 1 do
          for y = 0 to s1.height - 1 do
            if (Array2.unsafe_get s1.cells x y <> Array2.unsafe_get s2.cells x y) then raise Found
          done
        done;
        true
      with Found -> false end
    | true, true when s1.view = s2.view && s1.view_topright = s2.view_topright ->
      begin
        let x1, y1 = s1.view in
        let x2, y2 = s1.view_topright in
        try
          for x = 0 to x2 - x1 - 1 do
            for y = 0 to y2 - y1 - 1 do
              if (Array2.unsafe_get s1.cells x y <> Array2.unsafe_get s2.cells x y) then raise Found
            done
          done;
          true
        with Found -> false end
    | _ -> false

let equals2 s1 s2 = equals s1 s2 && s1.step = s2.step && s1.in_water = s2.in_water

exception Found_val of int

let compare s1 s2 =
  if s1.width <> s2.width then s1.width - s2.width
  else if s1.height <> s2.height then s1.height - s2.height
  else if s1.view <> s2.view then compare s1.view s2.view
  else if s1.view_topright <> s2.view_topright then compare s1.view_topright s2.view_topright
  else
    let x1, y1 = s1.view in
    let x2, y2 = s1.view_topright in
    try
      for x = 0 to x2 - x1 - 1 do
        for y = 0 to y2 - y1 - 1 do
          let c1 = Array2.unsafe_get s1.cells x y
          and c2 = Array2.unsafe_get s2.cells x y in
          if c1 <> c2 then raise (Found_val (int_of_char c1 - int_of_char c2))
        done
      done;
      0
    with Found_val x -> x

let create width height =
  let cells = Array2.create char c_layout width height in
  Array2.fill cells ' ';
  {
  cells;
  width;
  height;
  lift_locs = [];
  num_lambda = 0;
  robot_loc = 0, 0;
  flooding = 0;
  waterproof = 10;
  water = 0;
  num_got_lambdas = 0;
  step = 0;
  in_water = 0;
  is_above_rock = false;
  is_dead = false;
  trampolines = [];
  targets = [];
  warps = [];
  is_view = false;
  view = 0, 0;
  view_topright = width, height;
  growth = 25;
  razors = 0;
  growth_count = [];
  num_visible_lambdas = 0;
  }

let get state (x, y) =
  try let char =
        if state.is_view then let x1, y1 = state.view in Array2.get state.cells (x - x1) (y - y1)
        else Array2.get state.cells x y in
      char_to_cell char
  with Invalid_argument _ -> Out_of_map

let set state (x, y) c =
  try
    if state.is_view then let x1, y1 = state.view in Array2.set state.cells (x - x1) (y - y1) (cell_to_char c)
    else Array2.set state.cells x y (cell_to_char c)
  with Invalid_argument _ -> raise Setting_out_of_map

let copy state =
  let new_cells = Array2.create char c_layout state.width state.height in
  Array2.blit state.cells new_cells;
  {state with cells = new_cells}

let make_view (x1, y1) (x2, y2) state =
  let new_cells = Array2.create char c_layout (x2 - x1) (y2 - y1) in
  for x = 0 to x2 - x1 - 1 do
    for y = 0 to y2 - y1 - 1 do
      Array2.set new_cells x y (Array2.get state.cells (x - x1) (y - y1))
    done
  done;
  { state with
    cells = new_cells;
    is_view = true;
    view = x1, y1;
    view_topright = x2, y2;
  }

type command =
  | L
  | R
  | U
  | D
  | W
  | A
  | S

let command_list = [L; R; U; D; W; A; S]

let move_command_list = [L; R; U; D]

let special_command_list = [A; S]

let char_to_command = function
  | 'L' -> L
  | 'R' -> R
  | 'U' -> U
  | 'D' -> D
  | 'W' -> W
  | 'A' -> A
  | 'S' -> S
  | _ -> raise (Invalid_argument "char_to_command")

let command_to_char = function
  | L -> 'L'
  | R -> 'R'
  | U -> 'U'
  | D -> 'D'
  | W -> 'W'
  | A -> 'A'
  | S -> 'S'

let dest (x, y) = function
  | L -> x - 1, y
  | R -> x + 1, y
  | U -> x, y + 1
  | D -> x, y - 1
  | _ -> x, y

type move_result =
  | Moved
  | Waited

type refresh_result =
  | Continue
  | Aborted
  | Won
  | Lose
  | Already_dead

let refresh_result_to_string = function
  | Continue -> "Continue"
  | Aborted -> "Aborted"
  | Won -> "Won"
  | Lose -> "Lose"
  | Already_dead -> "Already_dead"

let cell_to_string = function
  | Robot -> "Robot"
  | Wall -> "Wall"
  | Rock -> "Rock"
  | Lambda -> "Lambda"
  | Closed -> "Closed"
  | Open -> "Open"
  | Earth -> "Earth"
  | Empty -> "Empty"
  | Trampoline c -> Printf.sprintf "Trampoline_%c" c
  | Target c -> Printf.sprintf "Target_%c" c
  | Beard -> "Beard"
  | Razor -> "Razor"
  | HOR -> "HOR"
  | Out_of_map -> "Out_of_map"
