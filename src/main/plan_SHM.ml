open Types
open Plan__cond
open Plan__move

let a = (1, 1)
let b = (4, 1)

let plan_SHM state =
  if is_loc a state then dijkstra_subj b state
  else if is_loc b state then dijkstra_subj a state
  else [W]
