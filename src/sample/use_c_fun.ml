external addxxx : string -> string = "addxxx"
external inc : int -> int = "inc"
external mult2 : float -> float = "mult2"

external sum_int_array : int array -> int = "sum_int_array"
external sum_int_tuple : int * int -> int = "sum_int_tuple"
external sum_float_array : float array -> float = "sum_float_array"
external sum_int_list : int list -> int = "sum_int_list"

external make_array : 'a -> int -> 'a array = "make_array"
external make_tuple : 'a -> 'a * 'a * 'a = "make_tuple"
external make_float_array : float -> int -> float array = "make_float_array"
external make_list : 'a -> int -> 'a list = "make_list"

type v = A | B of int | C of float | D
external print_variant : v -> unit = "print_variant"

let f () =
  let open Printf in
      printf "%s\n" (addxxx "a"); (* axxx *)
      printf "%d\n" (inc 9); (* 10 *)
      printf "%f\n\n" (mult2 1.2); (* 2.4 *)
      printf "%d\n" (sum_int_array [|1;2;3|]); (* 6 *)
      printf "%d\n" (sum_int_tuple (2, 4)); (* 6 *)
      printf "%f\n" (sum_float_array [|1.0;2.0;3.0|]); (* 6.0 *)
      printf "%d\n\n" (sum_int_list [1;2;3]); (* 6 *)
      Array.iter (printf "%d ") (make_array 5 3); print_newline (); (* 5 5 5 *)
      let a, b, c = make_tuple 5 in printf "%d, %d, %d\n" a b c; (* 5, 5, 5 *)
      Array.iter (printf "%f ") (make_float_array 5. 3); print_newline (); (* 5. 5. 5. *)
      List.iter (printf "%d ") (make_list 5 3); print_newline (); (* 5 5 5 *)
      print_newline ();
      print_variant A;
      print_variant (B 1);
      print_variant (C 2.);
      print_variant D

